//Import statements
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { addNewCourse } from '../reducers/courseReducer';
import { Formik, Form } from 'formik';
import { TextInput } from './FormikMuiFields';
import { notify } from '../reducers/notificationReducer';
import AlertMessage from './AlertMessage';
import * as yup from 'yup';
import getErrorMsg from '../utils/getErrorMsg';

import { useSubredditFormStyles } from '../styles/muiStyles';
import { Button, Typography } from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import AddIcon from '@material-ui/icons/Add';

//validationSchema component to set validations for Course Form 
const validationSchema = yup.object({
  courseName: yup
    .string()
    .required('Required')
    .max(100, 'Must be at most 100 characters')
    .min(3, 'Must be at least 3 characters')
    .matches(
      /^[a-zA-Z0-9-_]*$/,
      'Only alphanumeric characters allowed, no spaces/symbols'
    ),
  description: yup
    .string()
    .required('Required')
    .max(300, 'Must be at most 100 characters')
    .min(3, 'Must be at least 3 characters'),

  fee: yup
    .number()
    .required('Required'),

  enrolment: yup
    .string()
    .required('Required'),
});

//CourseForm Functional Compoenent
const CourseForm = () => {
  const [error, setError] = useState(null);
  const dispatch = useDispatch();
  const classes = useSubredditFormStyles();
  const history = useHistory();

  const handleCreateCourse = async (values, { setSubmitting }) => {
    try {
      setSubmitting(true);
      await dispatch(addNewCourse(values));
      setSubmitting(false);
      dispatch(
        notify(`New Course created: c/${values.courseName}`, 'success')
      );
      history.push(`/c/${values.courseName}`);
    } catch (err) {
      setSubmitting(false);
      dispatch(notify(getErrorMsg(err), 'error'));
    }
  };

  return (
    <div className={classes.formWrapper}>
      <Formik
        validateOnChange={true}
        initialValues={{ courseName: '', description: '', fee: '', duration: '', enrolment: '' }}
        onSubmit={handleCreateCourse}
        validationSchema={validationSchema}
      >
        {({ isSubmitting }) => (
          <Form className={classes.form}>
            <div className={classes.input}>
              <Typography
                className={classes.inputIconText}
                color="primary"
                variant="h5"
              >
                c/
              </Typography>
              <TextInput
                name="courseName"
                type="text"
                placeholder="Enter course name"
                label="Course Name"
                required
                fullWidth
              />
            </div>

            <div className={classes.descInput}>
              <InfoIcon className={classes.inputIcon} color="primary" />
              <TextInput
                name="description"
                type="text"
                placeholder="Enter description"
                label="Description"
                required
                fullWidth
                variant="outlined"
                multiline
                rows={2}
                maxRows={Infinity}
              />
            </div>
            <div className={classes.descInput}>
              <InfoIcon className={classes.inputIcon} color="primary" />
              <TextInput
                name="fee"
                type="number"
                placeholder="Enter course fee"
                label="Fee($)"
                required
                fullWidth
                variant="outlined"
                multiline
                rows={1}
              />
            </div>
            <div className={classes.descInput}>
              <InfoIcon className={classes.inputIcon} color="primary" />
              <TextInput
                name="duration"
                type="string"
                placeholder="Enter duration"
                label="Duration"
                required
                fullWidth
                variant="outlined"
                multiline
                rows={1}
              />
            </div>
            <div className={classes.descInput}>
              <InfoIcon className={classes.inputIcon} color="primary" />
              <TextInput
                name="enrolment"
                type="text"
                placeholder="Enter enrolment details"
                label="Enrolment starts:"
                required
                fullWidth
                variant="outlined"
                rows={1}
              />
            </div>
            <Button
              type="submit"
              color="secondary"
              variant="contained"
              size="large"
              className={classes.submitButton}
              disabled={isSubmitting}
              startIcon={<AddIcon />}
            >
              {isSubmitting ? 'Creating' : 'Create a Course Page'}
            </Button>
          </Form>
        )}
      </Formik>
      <AlertMessage
        error={error}
        severity="error"
        clearError={() => setError(null)}
      />
    </div>
  );
};

//Exporting CourseForm Component
export default CourseForm;
