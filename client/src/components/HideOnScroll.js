//import statements
import React from 'react';

import { useScrollTrigger, Slide } from '@material-ui/core';

//HideOnScroll Component
const HideOnScroll = ({ children }) => {
  const trigger = useScrollTrigger();

  return (
    <Slide appear={false} direction="up" in={!trigger}>
      {children}
    </Slide>
  );
};

//exporting HideOnScroll Component
export default HideOnScroll;
