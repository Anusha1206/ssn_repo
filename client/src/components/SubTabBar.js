//import statements
import React, { useState } from 'react';
import { ReactComponent as Hot } from '../svg/hot.svg';
import { Paper, Tabs, Tab, SvgIcon } from '@material-ui/core';
import { useSortTabStyles } from '../styles/muiStyles';

//SubTabBar Component
const SubTabBar = ({ user }) => {
  const classes = useSortTabStyles();
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <Paper variant="outlined" className={classes.mainPaper}>
      <Tabs selectedIndex={tabIndex} onSelect={index => setTabIndex(index)}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab
          icon={
            <SvgIcon fontSize="small">
              <Hot />
            </SvgIcon>
          }
          label="Posts"
        />
      </Tabs>
    </Paper>
  );
};

//exporting SubTabBar Component
export default SubTabBar;
