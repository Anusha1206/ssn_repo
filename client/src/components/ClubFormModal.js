//importing react and other components
import React, { useState } from 'react';
import ClubForm from './ClubForm';

import { DialogTitle } from './CustomDialogTitle';
import {
  Dialog,
  DialogContent,
  Button,
  MenuItem,
  ListItemIcon,
} from '@material-ui/core';
import { useDialogStyles } from '../styles/muiStyles';
import AddCircleIcon from '@material-ui/icons/AddCircle';

//ClubFormModal Component
const ClubFormModal = ({ type, handleCloseMenu }) => {
  const classes = useDialogStyles();
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpenMenu = () => {
    handleClickOpen();
    handleCloseMenu();
  };

  return (
    <div>
      {type !== 'menu' ? (
        <Button
          color="primary"
          variant="contained"
          onClick={handleClickOpen}
          fullWidth
          className={classes.createSubBtn}
          size="large"
          startIcon={<AddCircleIcon />}
        >
          Create a new club page
        </Button>
      ) : (
        <MenuItem onClick={handleOpenMenu}>
          <ListItemIcon>
            <AddCircleIcon style={{ marginRight: 7 }} />
            Create a new club page
          </ListItemIcon>
        </MenuItem>
      )}
      <Dialog
        open={open}
        onClose={handleClose}
        maxWidth="sm"
        classes={{ paper: classes.dialogWrapper }}
        fullWidth
      >
        <DialogTitle onClose={handleClose}>Create a new club page</DialogTitle>
        <DialogContent>
          <ClubForm />
        </DialogContent>
      </Dialog>
    </div>
  );
};
//exporting ClubFormModal
export default ClubFormModal;
