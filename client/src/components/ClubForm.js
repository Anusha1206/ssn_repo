//importing react and  other components 
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { addNewClub } from '../reducers/clubReducer';
import { Formik, Form } from 'formik';
import { TextInput } from './FormikMuiFields';
import { notify } from '../reducers/notificationReducer';
import AlertMessage from './AlertMessage';
import * as yup from 'yup';
import getErrorMsg from '../utils/getErrorMsg';

import { useSubredditFormStyles } from '../styles/muiStyles';
import { Button, Typography } from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import AddIcon from '@material-ui/icons/Add';

//validationSchema Component specifying validation criteria for ClubForm 
const validationSchema = yup.object({
  clubName: yup
    .string()
    .required('Required')
    .max(100, 'Must be at most 100 characters')
    .min(3, 'Must be at least 3 characters')
    .matches(
      /^[a-zA-Z0-9-_]*$/,
      'Only alphanumeric characters allowed, no spaces/symbols'
    ),
  description: yup
    .string()
    .required('Required')
    .max(300, 'Must be at most 100 characters')
    .min(3, 'Must be at least 3 characters'),

  phone: yup
    .number()
    .required('Required'),

  email: yup
    .string()
    .required('Required'),

  university: yup
    .string()
    .required('Required'),
});

//ClubForm Component
const ClubForm = () => {
  const [error, setError] = useState(null);
  const dispatch = useDispatch();
  const classes = useSubredditFormStyles();
  const history = useHistory();

  const handleCreateClub = async (values, { setSubmitting }) => {
    try {
      setSubmitting(true);
      await dispatch(addNewClub(values));
      setSubmitting(false);
      dispatch(
        notify(`New Club created: cl/${values.clubName}`, 'success')
      );
      history.push(`/cl/${values.clubName}`);
    } catch (err) {
      setSubmitting(false);
      dispatch(notify(getErrorMsg(err), 'error'));
    }
  };

  return (
    <div className={classes.formWrapper}>
      <Formik
        validateOnChange={true}
        initialValues={{ clubName: '', description: '', phone: '', email: '', university: '' }}
        onSubmit={handleCreateClub}
        validationSchema={validationSchema}
      >
        {({ isSubmitting }) => (
          <Form className={classes.form}>
            <div className={classes.input}>
              <Typography
                className={classes.inputIconText}
                color="primary"
                variant="h5"
              >
                cl/
              </Typography>
              <TextInput
                name="clubName"
                type="text"
                placeholder="Enter club name"
                label="Club Name"
                required
                fullWidth
              />
            </div>

            <div className={classes.descInput}>
              <InfoIcon className={classes.inputIcon} color="primary" />
              <TextInput
                name="description"
                type="text"
                placeholder="Enter description"
                label="Description"
                required
                fullWidth
                variant="outlined"
                multiline
                rows={2}
                maxRows={Infinity}
              />
            </div>
            <div className={classes.descInput}>
              <InfoIcon className={classes.inputIcon} color="primary" />
              <TextInput
                name="phone"
                type="number"
                placeholder="Enter phone"
                label="Phone"
                required
                fullWidth
                variant="outlined"
                multiline
                rows={1}
              />
            </div>
            <div className={classes.descInput}>
              <InfoIcon className={classes.inputIcon} color="primary" />
              <TextInput
                name="email"
                type="string"
                placeholder="Enter email"
                label="Email Id"
                required
                fullWidth
                variant="outlined"
                multiline
                rows={1}
              />
            </div>
            <div className={classes.descInput}>
              <InfoIcon className={classes.inputIcon} color="primary" />
              <TextInput
                name="university"
                type="text"
                placeholder="Enter university"
                label="University:"
                required
                fullWidth
                variant="outlined"
                rows={1}
              />
            </div>
            <Button
              type="submit"
              color="secondary"
              variant="contained"
              size="large"
              className={classes.submitButton}
              disabled={isSubmitting}
              startIcon={<AddIcon />}
            >
              {isSubmitting ? 'Creating' : 'Create a Club Page'}
            </Button>
          </Form>
        )}
      </Formik>
      <AlertMessage
        error={error}
        severity="error"
        clearError={() => setError(null)}
      />
    </div>
  );
};

//export ClubForm Component
export default ClubForm;
