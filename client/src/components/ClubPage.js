//import statements
import React, { useState, useEffect } from 'react';
import { useParams, Link as RouterLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchClub,
  toggleSubscribe,
  editDescription,
} from '../reducers/clubPageReducer';
import { notify } from '../reducers/notificationReducer';
import ErrorPage from './ErrorPage';
import LoadingSpinner from './LoadingSpinner';
import getErrorMsg from '../utils/getErrorMsg';

import {
  Container,
  Paper,
  Typography,
  Button,
  Link,
  TextField,
} from '@material-ui/core';
import { useSubPageStyles } from '../styles/muiStyles';
import CakeIcon from '@material-ui/icons/Cake';
import PersonIcon from '@material-ui/icons/Person';
import CheckIcon from '@material-ui/icons/Check';
import GroupIcon from '@material-ui/icons/Group';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';

//ClubPage Compoenent
const ClubPage = () => {
  const classes = useSubPageStyles();
  const { club } = useParams();
  const dispatch = useDispatch();
  const { user, clubPage } = useSelector((state) => state);
  const [editOpen, setEditOpen] = useState(false);
  const [descInput, setDescInput] = useState('');
  const [emailInput, setEmailInput] = useState('');
  const [phoneInput, setPhoneInput] = useState('');
  const [uniInput, setUniInput] = useState('');
  const [pageLoading, setPageLoading] = useState(true);
  const [pageError, setPageError] = useState(null);

  useEffect(() => {
    const getClub = async () => {
      try {
        await dispatch(fetchClub(club, 'hot'));
        setPageLoading(false);
      } catch (err) {
        setPageError(getErrorMsg(err));
      }
    };
    getClub();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [club]);

  useEffect(() => {
    if (clubPage) {
      setDescInput(clubPage.clubDetails.description);
      setEmailInput(clubPage.clubDetails.email);
      setUniInput(clubPage.clubDetails.university);
      setPhoneInput(clubPage.clubDetails.phone);
    }
  }, [clubPage]);

  if (pageError) {
    return (
      <Container disableGutters>
        <Paper variant="outlined" className={classes.mainPaper}>
          <ErrorPage errorMsg={pageError} />
        </Paper>
      </Container>
    );
  }

  if (!clubPage || pageLoading) {
    return (
      <Container disableGutters>
        <Paper variant="outlined" className={classes.mainPaper}>
          <LoadingSpinner text={'Fetching club data...'} />
        </Paper>
      </Container>
    );
  }

  const {
    clubName,
    subscribedBy,
    subscriberCount,
    description,
    email,
    phone,
    university,
    admin,
    createdAt,
    id,
  } = clubPage.clubDetails;

  const isSubscribed = user && subscribedBy.includes(user.id);

  const handleSubJoin = async () => {
    try {
      let updatedSubscribedBy = [];

      if (isSubscribed) {
        updatedSubscribedBy = subscribedBy.filter((s) => s !== user.id);
      } else {
        updatedSubscribedBy = [...subscribedBy, user.id];
      }
      await dispatch(toggleSubscribe(id, updatedSubscribedBy));

      let message = isSubscribed
        ? `Unsubscribed from cl/${clubName}`
        : `Subscribed to cl/${clubName}!`;
      dispatch(notify(message, 'success'));
    } catch (err) {
      dispatch(notify(getErrorMsg(err), 'error'));
    }
  };

  const handleEditDescription = async () => {
    try {
      await dispatch(editDescription(id, descInput));
      setEditOpen(false);
      dispatch(
        notify(`Updated description of your club: cl/${clubName}`, 'success')
      );
    } catch (err) {
      dispatch(notify(getErrorMsg(err), 'error'));
    }
  };

  return (
    <Container disableGutters>
      <Paper variant="outlined" className={classes.subInfoWrapper}>
        <div className={classes.firstPanel}>
          <Typography variant="h6" color="secondary">
            cl/{clubName}
          </Typography>
          <div className={classes.description}>
            {!editOpen ? (
              <Typography variant="body1">{description}</Typography>
            ) : (
              <div className={classes.inputDiv}>
                <TextField
                  multiline
                  required
                  fullWidth
                  rows={2}
                  rowsMax={Infinity}
                  value={descInput}
                  onChange={(e) => setDescInput(e.target.value)}
                  variant="outlined"
                  size="small"
                />
                <div className={classes.submitBtns}>
                  <Button
                    onClick={() => setEditOpen(false)}
                    color="primary"
                    variant="outlined"
                    size="small"
                    className={classes.cancelBtn}
                    style={{ padding: '0.2em' }}
                  >
                    Cancel
                    </Button>
                  <Button
                    onClick={handleEditDescription}
                    color="primary"
                    variant="outlined"
                    size="small"
                    style={{ padding: '0.2em' }}
                  >
                    Update
                    </Button>
                </div>
              </div>
            )}
            {user && user.id === admin.id && !editOpen && (
              <Button
                onClick={() => setEditOpen((prevState) => !prevState)}
                size="small"
                variant="outlined"
                color="primary"
                style={{ padding: '0.2em', marginLeft: '0.5em' }}
                startIcon={<EditIcon />}
              >
                Edit
              </Button>
            )}
          </div>

          <div className={classes.description}>
            {!editOpen ? (
              <Typography variant="body1" color="secondary"><b>Email Id:</b> {email}</Typography>
            ) : (
              <div className={classes.inputDiv}>
                <TextField
                  multiline
                  required
                  fullWidth
                  rows={1}
                  rowsMax={Infinity}
                  value={emailInput}
                  onChange={(e) => setEmailInput(e.target.value)}
                  variant="outlined"
                  size="small"
                />
                <div className={classes.submitBtns}>
                  <Button
                    onClick={() => setEditOpen(false)}
                    color="primary"
                    variant="outlined"
                    size="small"
                    className={classes.cancelBtn}
                    style={{ padding: '0.2em' }}
                  >
                    Cancel
                    </Button>
                  <Button
                    onClick={handleEditDescription}
                    color="primary"
                    variant="outlined"
                    size="small"
                    style={{ padding: '0.2em' }}
                  >
                    Update
                    </Button>
                </div>
              </div>
            )}
            {user && user.id === admin.id && !editOpen && (
              <Button
                onClick={() => setEditOpen((prevState) => !prevState)}
                size="small"
                variant="outlined"
                color="primary"
                style={{ padding: '0.2em', marginLeft: '0.5em' }}
                startIcon={<EditIcon />}
              >
                Edit
              </Button>
            )}
          </div>

          <div className={classes.description}>
            {!editOpen ? (
              <Typography variant="body1" color="secondary"><b>Phone:</b> {phone} </Typography>
            ) : (
              <div className={classes.inputDiv}>
                <TextField
                  multiline
                  required
                  fullWidth
                  rows={1}
                  rowsMax={Infinity}
                  value={phoneInput}
                  onChange={(e) => setPhoneInput(e.target.value)}
                  variant="outlined"
                  size="small"
                />
                <div className={classes.submitBtns}>
                  <Button
                    onClick={() => setEditOpen(false)}
                    color="primary"
                    variant="outlined"
                    size="small"
                    className={classes.cancelBtn}
                    style={{ padding: '0.2em' }}
                  >
                    Cancel
                    </Button>
                  <Button
                    onClick={handleEditDescription}
                    color="primary"
                    variant="outlined"
                    size="small"
                    style={{ padding: '0.2em' }}
                  >
                    Update
                    </Button>
                </div>
              </div>
            )}
            {user && user.id === admin.id && !editOpen && (
              <Button
                onClick={() => setEditOpen((prevState) => !prevState)}
                size="small"
                variant="outlined"
                color="primary"
                style={{ padding: '0.2em', marginLeft: '0.5em' }}
                startIcon={<EditIcon />}
              >
                Edit
              </Button>
            )}
          </div>
          <div className={classes.description}>
            {!editOpen ? (
              <Typography variant="body1" color="secondary"><b>University: </b>{university}
              </Typography>
            ) : (
              <div className={classes.inputDiv}>
                <TextField
                  multiline
                  required
                  fullWidth
                  rows={1}
                  rowsMax={Infinity}
                  value={uniInput}
                  onChange={(e) => setUniInput(e.target.value)}
                  variant="outlined"
                  size="small"
                />
                <div className={classes.submitBtns}>
                  <Button
                    onClick={() => setEditOpen(false)}
                    color="primary"
                    variant="outlined"
                    size="small"
                    className={classes.cancelBtn}
                    style={{ padding: '0.2em' }}
                  >
                    Cancel
                    </Button>
                  <Button
                    onClick={handleEditDescription}
                    color="primary"
                    variant="outlined"
                    size="small"
                    style={{ padding: '0.2em' }}
                  >
                    Update
                    </Button>
                </div>
              </div>
            )}
            {user && user.id === admin.id && !editOpen && (
              <Button
                onClick={() => setEditOpen((prevState) => !prevState)}
                size="small"
                variant="outlined"
                color="primary"
                style={{ padding: '0.2em', marginLeft: '0.5em' }}
                startIcon={<EditIcon />}
              >
                Edit
              </Button>
            )}
          </div>
        </div>

        <div className={classes.secondPanel}>
          <Typography
            variant="body2"
            className={classes.iconText}
            color="secondary"
          >
            <CakeIcon style={{ marginTop: 0 }} /> Created
              {' ' +
              String(new Date(createdAt)).split(' ').slice(1, 4).join(' ')}
          </Typography>
          <Typography
            variant="body2"
            color="secondary"
            className={classes.iconText}
          >
            <PersonIcon style={{ marginRight: 5 }} />
              Admin:
              <Link
              component={RouterLink}
              to={`/u/${admin.username}`}
              style={{ marginLeft: '0.3em' }}
            >
              u/{admin.username}
            </Link>
          </Typography>
          <br /> <br />
          {user && (
            <Button
              color="primary"
              variant="contained"
              startIcon={isSubscribed ? <CheckIcon /> : <AddIcon />}
              className={classes.joinBtn}
              onClick={handleSubJoin}
            >
              {isSubscribed ? 'Subscribed' : 'Subscribe'}
            </Button>
          )}
          <Typography
            variant="body2"
            color="primary"
            className={classes.iconText}
          >
            <GroupIcon style={{ marginBottom: 0 }} />
            {subscriberCount} subscribers
            </Typography>
        </div>
      </Paper>
    </Container>
  );
};
//exporting ClubPage Component
export default ClubPage;
