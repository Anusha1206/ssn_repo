//imports statements
import React, { useState, useEffect } from 'react';
import { useParams, Link as RouterLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchCourse,
  toggleSubscribe,
  editDescription,
} from '../reducers/coursePageReducer';
import { notify } from '../reducers/notificationReducer';
import ErrorPage from './ErrorPage';
import LoadingSpinner from './LoadingSpinner';
import getErrorMsg from '../utils/getErrorMsg';

import {
  Container,
  Paper,
  Typography,
  Button,
  Link,
  TextField,
} from '@material-ui/core';
import { useSubPageStyles } from '../styles/muiStyles';
import CakeIcon from '@material-ui/icons/Cake';
import PersonIcon from '@material-ui/icons/Person';
import CheckIcon from '@material-ui/icons/Check';
import GroupIcon from '@material-ui/icons/Group';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';

//coursepage component
const CoursePage = () => {
  const classes = useSubPageStyles();
  const { course } = useParams();
  const dispatch = useDispatch();
  const { user, coursePage } = useSelector((state) => state);
  const [editOpen, setEditOpen] = useState(false);
  const [descInput, setDescInput] = useState('');
  const [feeInput, setFeeInput] = useState('');
  const [enrolInput, setEnrolInput] = useState('');
  const [durationInput, setDurationInput] = useState('');
  const [pageLoading, setPageLoading] = useState(true);
  const [pageError, setPageError] = useState(null);

  useEffect(() => {
    const getCourse = async () => {
      try {
        await dispatch(fetchCourse(course, 'hot'));
        setPageLoading(false);
      } catch (err) {
        setPageError(getErrorMsg(err));
      }
    };
    getCourse();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [course]);

  useEffect(() => {
    if (coursePage) {
      setDescInput(coursePage.courseDetails.description);
      setFeeInput(coursePage.courseDetails.fee);
      setDurationInput(coursePage.courseDetails.duration);
      setEnrolInput(coursePage.courseDetails.enrolment);
    }
  }, [coursePage]);

  if (pageError) {
    return (
      <Container disableGutters>
        <Paper variant="outlined" className={classes.mainPaper}>
          <ErrorPage errorMsg={pageError} />
        </Paper>
      </Container>
    );
  }

  if (!coursePage || pageLoading) {
    return (
      <Container disableGutters>
        <Paper variant="outlined" className={classes.mainPaper}>
          <LoadingSpinner text={'Fetching course data...'} />
        </Paper>
      </Container>
    );
  }

  const {
    courseName,
    subscribedBy,
    subscriberCount,
    description,
    fee,
    duration,
    enrolment,
    admin,
    createdAt,
    id,
  } = coursePage.courseDetails;

  const isSubscribed = user && subscribedBy.includes(user.id);

  const handleSubJoin = async () => {
    try {
      let updatedSubscribedBy = [];

      if (isSubscribed) {
        updatedSubscribedBy = subscribedBy.filter((s) => s !== user.id);
      } else {
        updatedSubscribedBy = [...subscribedBy, user.id];
      }
      await dispatch(toggleSubscribe(id, updatedSubscribedBy));

      let message = isSubscribed
        ? `Unsubscribed from c/${courseName}`
        : `Subscribed to c/${courseName}!`;
      dispatch(notify(message, 'success'));
    } catch (err) {
      dispatch(notify(getErrorMsg(err), 'error'));
    }
  };

  const handleEditDescription = async () => {
    try {
      await dispatch(editDescription(id, descInput));
      setEditOpen(false);
      dispatch(
        notify(`Updated description of your course: c/${courseName}`, 'success')
      );
    } catch (err) {
      dispatch(notify(getErrorMsg(err), 'error'));
    }
  };

  return (
    <Container disableGutters>
      <Paper variant="outlined" className={classes.subInfoWrapper}>
        <div className={classes.firstPanel}>
          <Typography variant="h6" color="secondary">
            c/{courseName}
          </Typography>
          <div className={classes.description}>
            {!editOpen ? (
              <Typography variant="body1">{description}</Typography>
            ) : (
              <div className={classes.inputDiv}>
                <TextField
                  multiline
                  required
                  fullWidth
                  rows={2}
                  rowsMax={Infinity}
                  value={descInput}
                  onChange={(e) => setDescInput(e.target.value)}
                  variant="outlined"
                  size="small"
                />
                <div className={classes.submitBtns}>
                  <Button
                    onClick={() => setEditOpen(false)}
                    color="primary"
                    variant="outlined"
                    size="small"
                    className={classes.cancelBtn}
                    style={{ padding: '0.2em' }}
                  >
                    Cancel
                    </Button>
                  <Button
                    onClick={handleEditDescription}
                    color="primary"
                    variant="outlined"
                    size="small"
                    style={{ padding: '0.2em' }}
                  >
                    Update
                    </Button>
                </div>
              </div>
            )}
            {user && user.id === admin.id && !editOpen && (
              <Button
                onClick={() => setEditOpen((prevState) => !prevState)}
                size="small"
                variant="outlined"
                color="primary"
                style={{ padding: '0.2em', marginLeft: '0.5em' }}
                startIcon={<EditIcon />}
              >
                Edit
              </Button>
            )}
          </div>

          <div className={classes.description}>
            {!editOpen ? (
              <Typography variant="body1" color="secondary"><b>Fee($):</b> {fee}</Typography>
            ) : (
              <div className={classes.inputDiv}>
                <TextField
                  multiline
                  required
                  fullWidth
                  rows={1}
                  rowsMax={Infinity}
                  value={feeInput}
                  onChange={(e) => setFeeInput(e.target.value)}
                  variant="outlined"
                  size="small"
                />
                <div className={classes.submitBtns}>
                  <Button
                    onClick={() => setEditOpen(false)}
                    color="primary"
                    variant="outlined"
                    size="small"
                    className={classes.cancelBtn}
                    style={{ padding: '0.2em' }}
                  >
                    Cancel
                    </Button>
                  <Button
                    onClick={handleEditDescription}
                    color="primary"
                    variant="outlined"
                    size="small"
                    style={{ padding: '0.2em' }}
                  >
                    Update
                    </Button>
                </div>
              </div>
            )}
            {user && user.id === admin.id && !editOpen && (
              <Button
                onClick={() => setEditOpen((prevState) => !prevState)}
                size="small"
                variant="outlined"
                color="primary"
                style={{ padding: '0.2em', marginLeft: '0.5em' }}
                startIcon={<EditIcon />}
              >
                Edit
              </Button>
            )}
          </div>

          <div className={classes.description}>
            {!editOpen ? (
              <Typography variant="body1" color="secondary"><b>Duration:</b> {duration} </Typography>
            ) : (
              <div className={classes.inputDiv}>
                <TextField
                  multiline
                  required
                  fullWidth
                  rows={1}
                  rowsMax={Infinity}
                  value={durationInput}
                  onChange={(e) => setDurationInput(e.target.value)}
                  variant="outlined"
                  size="small"
                />
                <div className={classes.submitBtns}>
                  <Button
                    onClick={() => setEditOpen(false)}
                    color="primary"
                    variant="outlined"
                    size="small"
                    className={classes.cancelBtn}
                    style={{ padding: '0.2em' }}
                  >
                    Cancel
                    </Button>
                  <Button
                    onClick={handleEditDescription}
                    color="primary"
                    variant="outlined"
                    size="small"
                    style={{ padding: '0.2em' }}
                  >
                    Update
                    </Button>
                </div>
              </div>
            )}
            {user && user.id === admin.id && !editOpen && (
              <Button
                onClick={() => setEditOpen((prevState) => !prevState)}
                size="small"
                variant="outlined"
                color="primary"
                style={{ padding: '0.2em', marginLeft: '0.5em' }}
                startIcon={<EditIcon />}
              >
                Edit
              </Button>
            )}
          </div>
          <div className={classes.description}>
            {!editOpen ? (
              <Typography variant="body1" color="secondary"><b>Enrolment Details: </b>{enrolment}
              </Typography>
            ) : (
              <div className={classes.inputDiv}>
                <TextField
                  multiline
                  required
                  fullWidth
                  rows={1}
                  rowsMax={Infinity}
                  value={enrolInput}
                  onChange={(e) => setEnrolInput(e.target.value)}
                  variant="outlined"
                  size="small"
                />
                <div className={classes.submitBtns}>
                  <Button
                    onClick={() => setEditOpen(false)}
                    color="primary"
                    variant="outlined"
                    size="small"
                    className={classes.cancelBtn}
                    style={{ padding: '0.2em' }}
                  >
                    Cancel
                    </Button>
                  <Button
                    onClick={handleEditDescription}
                    color="primary"
                    variant="outlined"
                    size="small"
                    style={{ padding: '0.2em' }}
                  >
                    Update
                    </Button>
                </div>
              </div>
            )}
            {user && user.id === admin.id && !editOpen && (
              <Button
                onClick={() => setEditOpen((prevState) => !prevState)}
                size="small"
                variant="outlined"
                color="primary"
                style={{ padding: '0.2em', marginLeft: '0.5em' }}
                startIcon={<EditIcon />}
              >
                Edit
              </Button>
            )}
          </div>
        </div>

        <div className={classes.secondPanel}>
          <Typography
            variant="body2"
            className={classes.iconText}
            color="secondary"
          >
            <CakeIcon style={{ marginTop: 0 }} /> Created
              {' ' +
              String(new Date(createdAt)).split(' ').slice(1, 4).join(' ')}
          </Typography>
          <Typography
            variant="body2"
            color="secondary"
            className={classes.iconText}
          >
            <PersonIcon style={{ marginRight: 5 }} />
              Admin:
              <Link
              component={RouterLink}
              to={`/u/${admin.username}`}
              style={{ marginLeft: '0.3em' }}
            >
              u/{admin.username}
            </Link>
          </Typography>
          <br /> <br />
          {user && (
            <Button
              color="primary"
              variant="contained"
              startIcon={isSubscribed ? <CheckIcon /> : <AddIcon />}
              className={classes.joinBtn}
              onClick={handleSubJoin}
            >
              {isSubscribed ? 'Subscribed' : 'Subscribe'}
            </Button>
          )}
          <Typography
            variant="body2"
            color="primary"
            className={classes.iconText}
          >
            <GroupIcon style={{ marginBottom: 0 }} />
            {subscriberCount} subscribers
            </Typography>
        </div>
      </Paper>
    </Container>
  );
};

//exporting CoursePage Component
export default CoursePage;
