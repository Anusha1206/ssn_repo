//import statements
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PostFormModal from './components/PostFormModal';
import PostList from './components/PostList';
import PostCommentsPage from './components/PostCommentsPage';
import UserPage from './components/UserPage';
import SubPage from './components/SubPage';
import CoursePage from './components/CoursePage';
import ClubPage from './components/ClubPage';
import TopSubsPanel from './components/TopSubsPanel';
import TopCoursesPanel from './components/TopCoursesPanel';
import TopClubsPanel from './components/TopClubsPanel';
import SearchResults from './components/SearchResults';
import NotFoundPage from './components/NotFoundPage';

import { Container } from '@material-ui/core/';
import { useMainPaperStyles } from './styles/muiStyles';

// All the routes for navigating to each page
const Routes = () => {
  const classes = useMainPaperStyles();

  return (
    <Switch>
      <Route exact path="/">
        <Container disableGutters className={classes.homepage}>
          <div className={classes.postsPanel}>
            <PostFormModal />
            <PostList />
          </div>
          <div className={classes.postsPanel}>
            <TopSubsPanel />
            <TopClubsPanel />
          </div>
        </Container>
      </Route>
      <Route exact path="/comments/:id">
        <PostCommentsPage />
      </Route>
      <Route exact path="/u/:username">
        <UserPage />
      </Route>
      <Route exact path="/uni/:sub">
        <Container disableGutters className={classes.homepage}>
          <SubPage />
          <TopCoursesPanel />
        </Container>
      </Route>
      <Route exact path="/c/:course">
        <Container disableGutters className={classes.homepage}>
          <CoursePage />
        </Container>
      </Route>
      <Route exact path="/cl/:club">
        <Container disableGutters className={classes.homepage}>
          <ClubPage />
        </Container>
      </Route>
      <Route exact path="/search/:query">
        <SearchResults />
      </Route>
      <Route>
        <NotFoundPage />
      </Route>
    </Switch>
  );
};

//exporting Routes
export default Routes;
