//import statements
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from './reducers/userReducer';
import { fetchPosts } from './reducers/postReducer';
import { setSubList, setTopSubsList } from './reducers/subReducer';
import { setCourseList, setTopCoursesList } from './reducers/courseReducer';
import { setClubList, setTopClubsList } from './reducers/clubReducer';
import { setDarkMode } from './reducers/themeReducer';
import { notify } from './reducers/notificationReducer';
import NavBar from './components/NavBar';
import ToastNotif from './components/ToastNotif';
import Routes from './Routes';
import getErrorMsg from './utils/getErrorMsg';

import { Paper } from '@material-ui/core/';
import customTheme from './styles/customTheme';
import { useMainPaperStyles } from './styles/muiStyles';
import { ThemeProvider } from '@material-ui/core/styles';

const App = () => {
  const classes = useMainPaperStyles();
  const dispatch = useDispatch();
  const { darkMode } = useSelector((state) => state);

  useEffect(() => {
    const setPostsAndSubreddits = async () => {
      try {
        await dispatch(fetchPosts('hot'));
        await dispatch(setSubList());
        await dispatch(setTopSubsList());

      } catch (err) {
        dispatch(notify(getErrorMsg(err), 'error'));
      }
    };
    const setCourses = async () => {
      try {
        await dispatch(setCourseList());
        await dispatch(setTopCoursesList());
        dispatch(setClubList());
        await dispatch(setTopClubsList());
      } catch (err) {
        dispatch(notify(getErrorMsg(err), 'error'));
      }
    };
    const setClubs = async () => {
      try {
        dispatch(setClubList());
        await dispatch(setTopClubsList());
      } catch (err) {
        dispatch(notify(getErrorMsg(err), 'error'));
      }
    };
    //calling the functions inside dispatch
    dispatch(setUser());
    dispatch(setDarkMode());
    setPostsAndSubreddits();
    setCourses();
    setClubs();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ThemeProvider theme={customTheme(darkMode)}>
      <Paper className={classes.root} elevation={0}>
        <ToastNotif />
        <NavBar />
        <Routes />
      </Paper>
    </ThemeProvider>
  );
};

//exporting App
export default App;
