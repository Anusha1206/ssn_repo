//import statement
import clubService from '../services/clubs';

//clubReducer Component
const clubReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_ALL_CLUBS_LIST':
      return { ...state, allClubs: action.payload };
    case 'SET_TOP_CLUBS_LIST':
      return { ...state, topClubs: action.payload };
    case 'SUBSCRIBE_CLUB_FROM_LIST':
      return {
        ...state,
        topClubs: state.topClubs.map((t) =>
          t.id !== action.payload.id ? t : { ...t, ...action.payload.data }
        ),
      };
    case 'ADD_NEW_CLUB':
      return {
        ...state,
        allClubs: [...state.allClubs, action.payload],
      };
    default:
      return state;
  }
};
//setClubList Component
export const setClubList = () => {
  return async (dispatch) => {
    const clubs = await clubService.getAllClubs();
    dispatch({
      type: 'SET_ALL_CLUBS_LIST',
      payload: clubs,
    });
  };
};

//setTopClubsList Component
export const setTopClubsList = () => {
  return async (dispatch) => {
    const top10Clubs = await clubService.getTopClubs();
    dispatch({
      type: 'SET_TOP_CLUBS_LIST',
      payload: top10Clubs,
    });
  };
};

//toggleSubscribe Component
export const toggleSubscribe = (id, subscribedBy) => {
  return async (dispatch) => {
    const subscriberCount = subscribedBy.length;

    dispatch({
      type: 'SUBSCRIBE_CLUB_FROM_LIST',
      payload: { id, data: { subscribedBy, subscriberCount } },
    });

    await clubService.subscribeClub(id);
  };
};

//addNewClub Component
export const addNewClub = (clubObj) => {
  return async (dispatch) => {
    const createdClub = await clubService.createClub(clubObj);
    dispatch({
      type: 'ADD_NEW_CLUB',
      payload: {
        clubName: createdClub.clubName,
        id: createdClub.id,
      },
    });
  };
};

//exporting clubReducer 
export default clubReducer;
