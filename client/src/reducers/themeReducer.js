//import statement
import storageService from '../utils/localStorage';

//themeReducer component
const themeReducer = (state = false, action) => {
  switch (action.type) {
    case 'TOGGLE_DARK_MODE':
      return !state;
    default:
      return state;
  }
};

//toggleDarkMode Component
export const toggleDarkMode = (isDarkMode) => {
  return (dispatch) => {
    storageService.saveDarkMode(isDarkMode);

    dispatch({ type: 'TOGGLE_DARK_MODE' });
  };
};

//setDarkMode Component
export const setDarkMode = () => {
  return (dispatch) => {
    const isDarkMode = storageService.loadDarkMode();

    if (isDarkMode === 'true') {
      dispatch({ type: 'TOGGLE_DARK_MODE' });
    }
  };
};

//exporting themeReducer Component
export default themeReducer;
