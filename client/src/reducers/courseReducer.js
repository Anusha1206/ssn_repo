//import statement
import courseService from '../services/courses';

//courseReducer Component
const courseReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_ALL_COURSES_LIST':
      return { ...state, allCourses: action.payload };
    case 'SET_TOP_COURSES_LIST':
      return { ...state, topCourses: action.payload };
    case 'SUBSCRIBE_COURSE_FROM_LIST':
      return {
        ...state,
        topCourses: state.topCourses.map((t) =>
          t.id !== action.payload.id ? t : { ...t, ...action.payload.data }
        ),
      };
    case 'ADD_NEW_COURSE':
      return {
        ...state,
        allCourses: [...state.allCourses, action.payload],
      };
    default:
      return state;
  }
};

// setCourseList Component
export const setCourseList = () => {
  return async (dispatch) => {
    const courses = await courseService.getAllCourses();
    dispatch({
      type: 'SET_ALL_COURSES_LIST',
      payload: courses,
    });
  };
};

//setTopCoursesList Component
export const setTopCoursesList = () => {
  return async (dispatch) => {
    const top10Courses = await courseService.getTopCourses();
    dispatch({
      type: 'SET_TOP_COURSES_LIST',
      payload: top10Courses,
    });
  };
};

//toggleSubscribe Component
export const toggleSubscribe = (id, subscribedBy) => {
  return async (dispatch) => {
    const subscriberCount = subscribedBy.length;

    dispatch({
      type: 'SUBSCRIBE_COURSE_FROM_LIST',
      payload: { id, data: { subscribedBy, subscriberCount } },
    });

    await courseService.subscribeCourse(id);
  };
};

//addNewCourse Component
export const addNewCourse = (courseObj) => {
  return async (dispatch) => {
    const createdCourse = await courseService.createCourse(courseObj);
    dispatch({
      type: 'ADD_NEW_COURSE',
      payload: {
        courseName: createdCourse.courseName,
        id: createdCourse.id,
      },
    });
  };
};

//exporting courseReducer Component
export default courseReducer;
