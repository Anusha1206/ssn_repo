//import statement
import postService from '../services/posts';

// searchReducer Component
const searchReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_SEARCH_RESULTS':
      return action.payload;
    case 'TOGGLE_SEARCH_VOTE':
      return {
        ...state,
        results: state.results.map((r) =>
          r.id !== action.payload.id ? r : { ...r, ...action.payload.data }
        ),
      };
    case 'LOAD_SEARCH_POSTS':
      return {
        ...action.payload,
        results: [...state.results, ...action.payload.results],
      };
    default:
      return state;
  }
};

//setSearchResults Component
export const setSearchResults = (query) => {
  return async (dispatch) => {
    const results = await postService.getSearchResults(query, 10, 1);

    dispatch({
      type: 'SET_SEARCH_RESULTS',
      payload: results,
    });
  };
};

//loadSearchPosts Component
export const loadSearchPosts = (query, page) => {
  return async (dispatch) => {
    const results = await postService.getSearchResults(query, 10, page);

    dispatch({
      type: 'LOAD_SEARCH_POSTS',
      payload: results,
    });
  };
};

//toggleUpvote Component
export const toggleUpvote = (id, upvotedBy, downvotedBy) => {
  return async (dispatch) => {
    let pointsCount = upvotedBy.length - downvotedBy.length;
    if (pointsCount < 0) {
      pointsCount = 0;
    }

    dispatch({
      type: 'TOGGLE_SEARCH_VOTE',
      payload: { id, data: { upvotedBy, pointsCount, downvotedBy } },
    });

    await postService.upvotePost(id);
  };
};

// toggleDownvote Component
export const toggleDownvote = (id, downvotedBy, upvotedBy) => {
  return async (dispatch) => {
    let pointsCount = upvotedBy.length - downvotedBy.length;
    if (pointsCount < 0) {
      pointsCount = 0;
    }

    dispatch({
      type: 'TOGGLE_SEARCH_VOTE',
      payload: { id, data: { upvotedBy, pointsCount, downvotedBy } },
    });

    await postService.downvotePost(id);
  };
};

//exporting searchReducer Component
export default searchReducer;
