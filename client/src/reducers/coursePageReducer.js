//import statement
import courseService from '../services/courses';

//coursePageReducer Component
const coursePageReducer = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_COURSE':
      return action.payload;

    case 'SUBSCRIBE_COURSE':
      return {
        ...state,
        courseDetails: { ...state.courseDetails, ...action.payload },
      };
    case 'EDIT_DESCRIPTION':
      return {
        ...state,
        courseDetails: { ...state.courseDetails, description: action.payload },
      };
    default:
      return state;
  }
};

//fetchCourse Component
export const fetchCourse = (courseName, sortBy) => {
  return async (dispatch) => {
    const course = await courseService.getCourse(courseName, sortBy, 10, 1);

    dispatch({
      type: 'FETCH_COURSE',
      payload: course,
    });
  };
};

//toggleSubscribe Component
export const toggleSubscribe = (id, subscribedBy) => {
  return async (dispatch) => {
    const subscriberCount = subscribedBy.length;

    dispatch({
      type: 'SUBSCRIBE_COURSE',
      payload: { subscribedBy, subscriberCount },
    });

    await courseService.subscribeCourse(id);
  };
};
//editDescription Component
export const editDescription = (id, description) => {
  return async (dispatch) => {
    await courseService.updateDescription(id, { description });

    dispatch({
      type: 'EDIT_DESCRIPTION',
      payload: description,
    });
  };
};

//exporting editDescription
export default coursePageReducer;
