//import statements
import clubService from '../services/clubs';

//clubPageReducer Component
const clubPageReducer = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_CLUB':
      return action.payload;

    case 'SUBSCRIBE_CLUB':
      return {
        ...state,
        clubDetails: { ...state.clubDetails, ...action.payload },
      };
    case 'EDIT_DESCRIPTION':
      return {
        ...state,
        clubDetails: { ...state.clubDetails, description: action.payload },
      };
    default:
      return state;
  }
};

// fetchClub Component
export const fetchClub = (clubName, sortBy) => {
  return async (dispatch) => {
    const club = await clubService.getClub(clubName, sortBy, 10, 1);

    dispatch({
      type: 'FETCH_CLUB',
      payload: club,
    });
  };
};

//toggleSubscribe Component
export const toggleSubscribe = (id, subscribedBy) => {
  return async (dispatch) => {
    const subscriberCount = subscribedBy.length;

    dispatch({
      type: 'SUBSCRIBE_CLUB',
      payload: { subscribedBy, subscriberCount },
    });

    await clubService.subscribeClub(id);
  };
};

//editDescription Component
export const editDescription = (id, description) => {
  return async (dispatch) => {
    await clubService.updateDescription(id, { description });

    dispatch({
      type: 'EDIT_DESCRIPTION',
      payload: description,
    });
  };
};

//exporting clubPageReducer Component
export default clubPageReducer;
