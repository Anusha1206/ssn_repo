//import statements
import axios from 'axios';
import backendUrl from '../backendUrl';
import { token } from './auth';

const baseUrl = `${backendUrl}/api/courses`;

const setConfig = () => {
  return {
    headers: { 'x-auth-token': token },
  };
};

//getAllCourses Component
const getAllCourses = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
};

//getCourse Component
const getCourse = async (courseName, sortBy, limit, page) => {
  const response = await axios.get(
    `${baseUrl}/c/${courseName}/?sortby=${sortBy}&limit=${limit}&page=${page}`
  );
  return response.data;
};

//createCourse Component
const createCourse = async (courseObj) => {
  const response = await axios.post(`${baseUrl}`, courseObj, setConfig());
  return response.data;
};

//subscribeCourse Component
const subscribeCourse = async (id) => {
  const response = await axios.post(
    `${baseUrl}/${id}/subscribe`,
    null,
    setConfig()
  );
  return response.data;
};

//updateDescription component
const updateDescription = async (id, descriptionObj) => {
  const response = await axios.patch(
    `${baseUrl}/${id}`,
    descriptionObj,
    setConfig()
  );
  return response.data;
};

//getTopCourses Component
const getTopCourses = async () => {
  const response = await axios.get(`${baseUrl}/top10`);
  return response.data;
};

const courseService = {
  getAllCourses,
  createCourse,
  getCourse,
  subscribeCourse,
  updateDescription,
  getTopCourses,
};

//exporting courseService Component
export default courseService;
