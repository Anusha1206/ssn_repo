//import statements
import axios from 'axios';
import backendUrl from '../backendUrl';

export let token = null;

const setToken = (newToken) => {
  token = newToken;
};

//login component
const login = async (credentials) => {
  const response = await axios.post(`${backendUrl}/api/login`, credentials);
  return response.data;
};

//sugnup component
const signup = async (enteredData) => {
  const response = await axios.post(`${backendUrl}/api/signup`, enteredData);
  return response.data;
};

const authService = { setToken, login, signup };

//exporting authService Component
export default authService;
