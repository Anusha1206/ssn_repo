//import statements
import axios from 'axios';
import backendUrl from '../backendUrl';
import { token } from './auth';

const baseUrl = `${backendUrl}/api/clubs`;

const setConfig = () => {
  return {
    headers: { 'x-auth-token': token },
  };
};

//getAllClubs Component
const getAllClubs = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
};

//getClub Component
const getClub = async (clubName, sortBy, limit, page) => {
  const response = await axios.get(
    `${baseUrl}/cl/${clubName}/?sortby=${sortBy}&limit=${limit}&page=${page}`
  );
  return response.data;
};

//createClub Component
const createClub = async (clubObj) => {
  const response = await axios.post(`${baseUrl}`, clubObj, setConfig());
  return response.data;
};

//subscribeClub Component
const subscribeClub = async (id) => {
  const response = await axios.post(
    `${baseUrl}/${id}/subscribe`,
    null,
    setConfig()
  );
  return response.data;
};

//updateDescription Component
const updateDescription = async (id, descriptionObj) => {
  const response = await axios.patch(
    `${baseUrl}/${id}`,
    descriptionObj,
    setConfig()
  );
  return response.data;
};

//getTopClubs Component
const getTopClubs = async () => {
  const response = await axios.get(`${baseUrl}/top10`);
  return response.data;
};

const clubService = {
  getAllClubs,
  createClub,
  getClub,
  subscribeClub,
  updateDescription,
  getTopClubs,
};

//exporting ClubService
export default clubService;
