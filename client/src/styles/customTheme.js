//import statement
import { createMuiTheme } from '@material-ui/core/styles';

//customTheme Component
const customTheme = (darkMode) =>
  createMuiTheme({
    palette: {
      type: darkMode ? 'dark' : 'light',
      primary: {
        main: darkMode ? '#ffb28a' : '#2874a6',
      },
      secondary: {
        main: darkMode ? '#f3b9bb' : '#145a32',
      },
    },
    overrides: {
      MuiTypography: {
        root: {
          wordBreak: 'break-word',
        },
      },
    },
  });

//export customTheme Component  
export default customTheme;
