//course schema
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const schemaCleaner = require('../utils/schemaCleaner');

const courseSchema = new mongoose.Schema(
  {
    courseName: {
      type: String,
      minlength: 3,
      maxlength: 50,
      required: true,
      trim: true,
    },
    description: {
      type: String,
      minlength: 3,
      maxlength: 300,
      required: true,
      trim: true,
    },
    fee: {
      type: Number,
      required: true,
    },
    enrolment: {
      type: String,
      required: true,
    },
    duration: {
      type: String,
      required: true,
    },
    posts: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post',
      },
    ],
    admin: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    subscribedBy: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
    subscriberCount: {
      type: Number,
      default: 1,
    },

  },
  {
    timestamps: true,
  }
);

courseSchema.plugin(uniqueValidator);

// replaces _id with id, convert id to string from ObjectID and deletes __v
schemaCleaner(courseSchema);

module.exports = mongoose.model('Course', courseSchema);
