//club schema
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const schemaCleaner = require('../utils/schemaCleaner');

const clubSchema = new mongoose.Schema(
  {
    clubName: {
      type: String,
      minlength: 3,
      maxlength: 50,
      required: true,
      trim: true,
    },
    description: {
      type: String,
      minlength: 3,
      maxlength: 300,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      minlength: 3,
      maxlength: 200,
      required: true,
      trim: true,
    },
    phone: {
      type: Number,
      required: true,
    },
    university:
    {
      type: String,
      required: true,
    },
    admin: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    subscribedBy: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
    subscriberCount: {
      type: Number,
      default: 1,
    },
  },
  {
    timestamps: true,
  }
);

clubSchema.plugin(uniqueValidator);

// replaces _id with id, convert id to string from ObjectID and deletes __v
schemaCleaner(clubSchema);

module.exports = mongoose.model('Clubs', clubSchema);
