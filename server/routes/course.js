const express = require('express');
const { auth } = require('../utils/middleware');
const {
  getAllCourses,
  getCourseDetails,
  createNewCourse,
  getTopCourses,
  editCourseDescription,
  subscribeToCourse,
} = require('../controllers/course');

const router = express.Router();

// routes for courses
router.get('/', getAllCourses);
router.get('/c/:courseName', getCourseDetails);
router.post('/', auth, createNewCourse);
router.patch('/:id', auth, editCourseDescription);
router.get('/top10', getTopCourses);
router.post('/:id/subscribe', auth, subscribeToCourse);

module.exports = router;