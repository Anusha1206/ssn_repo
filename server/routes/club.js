const express = require('express');
const { auth } = require('../utils/middleware');
const {
  getAllClubs,
  getClubDetails,
  createNewClub,
  getTopClubs,
  editClubDescription,
  subscribeToClub,
} = require('../controllers/club');

const router = express.Router();

// routes for club 
router.get('/', getAllClubs);
router.get('/cl/:clubName', getClubDetails);
router.post('/', auth, createNewClub);
router.get('/top10', getTopClubs);
router.patch('/:id', auth, editClubDescription);
router.post('/:id/subscribe', auth, subscribeToClub);

module.exports = router;