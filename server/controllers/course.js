const Course = require('../models/course');
const User = require('../models/user');

//Fucntion to get all courses
const getAllCourses = async (_req, res) => {
  const allCourses = await Course.find({}).select('id courseName');
  res.status(200).json(allCourses);
};
// To get course details
const getCourseDetails = async (req, res) => {
  const { courseName } = req.params;
  const page = Number(req.query.page);
  const limit = Number(req.query.limit);
  const sortBy = req.query.sortby;

  let sortQuery;
  switch (sortBy) {
    case 'new':
      sortQuery = { createdAt: -1 };
      break;
    case 'top':
      sortQuery = { pointsCount: -1 };
      break;
    case 'best':
      sortQuery = { voteRatio: -1 };
      break;
    case 'hot':
      sortQuery = { hotAlgo: -1 };
      break;
    case 'controversial':
      sortQuery = { controversialAlgo: -1 };
      break;
    case 'old':
      sortQuery = { createdAt: 1 };
      break;
    default:
      sortQuery = {};
  }

  const course = await Course.findOne({
    courseName: { $regex: new RegExp('^' + courseName + '$', 'i') },
  }).populate('admin', 'username');

  if (!course) {
    return res.status(404).send({
      message: `Course '${courseName}' does not exist on server.`,
    });
  }

  res.status(200).json({ courseDetails: course });
};

//Fucntion to retrieve top courses
const getTopCourses = async (_req, res) => {
  const top10Courses = await Course.find({})
    .sort({ subscriberCount: -1 })
    .limit(3)
    .select('-description -admin');
  // .select('-description -posts -admin ');

  res.status(200).json(top10Courses);
};

// To create new course
const createNewCourse = async (req, res) => {
  const { courseName, description, fee, enrolment, duration } = req.body;

  const admin = await User.findById(req.user);
  if (!admin) {
    return res
      .status(404)
      .send({ message: 'User does not exist in database.' });
  }

  const existingCourseName = await Course.findOne({
    courseName: { $regex: new RegExp('^' + courseName + '$', 'i') },
  });

  if (existingCourseName) {
    return res.status(403).send({
      message: `Course having same name "${courseName}" already exists.`,
    });
  }

  const newCourse = new Course({
    courseName,
    description,
    fee,
    enrolment,
    duration,
    admin: admin._id,
    subscribedBy: [admin._id],
    subscriberCount: 1,
  });

  /*  const subscribedCourse = await Course.find({
     _id: { $in: user.subscribedCourse },
   }); */

  const savedCourse = await newCourse.save();

  admin.subscribedSubs = admin.subscribedSubs.concat(savedCourse._id);
  await admin.save();

  return res.status(201).json(savedCourse);
};

// To edit course description
const editCourseDescription = async (req, res) => {
  const { description } = req.body;
  const { id } = req.params;

  if (!description) {
    return res
      .status(400)
      .send({ message: `Description body can't be empty.` });
  }

  const admin = await User.findById(req.user);
  const course = await Course.findById(id);

  if (!admin) {
    return res
      .status(404)
      .send({ message: 'User does not exist in database.' });
  }

  if (!course) {
    return res.status(404).send({
      message: `Course with ID: ${id} does not exist in database.`,
    });
  }

  if (course.admin.toString() !== admin._id.toString()) {
    return res.status(401).send({ message: 'Access is denied.' });
  }

  course.description = description;

  await course.save();
  res.status(202).end();
};

// To subscribe a course
const subscribeToCourse = async (req, res) => {
  const { id } = req.params;

  const course = await Course.findById(id);
  const user = await User.findById(req.user);

  if (course.subscribedBy.includes(user._id.toString())) {
    course.subscribedBy = course.subscribedBy.filter(
      (s) => s.toString() !== user._id.toString()
    );

    user.subscribedSubs = user.subscribedSubs.filter(
      (s) => s.toString() !== course._id.toString()
    );
  } else {
    course.subscribedBy = course.subscribedBy.concat(user._id);

    user.subscribedSubs = user.subscribedSubs.concat(course._id);
  }

  course.subscriberCount = course.subscribedBy.length;

  await course.save();
  await user.save();

  res.status(201).end();
};

module.exports = {
  getAllCourses,
  getCourseDetails,
  createNewCourse,
  getTopCourses,
  editCourseDescription,
  subscribeToCourse,
};
