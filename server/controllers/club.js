const Club = require('../models/club');
const User = require('../models/user');

//Fucntion to get all clubs
const getAllClubs = async (_req, res) => {
  const allClubs = await Club.find({}).select('id clubName');
  res.status(200).json(allClubs);
};
// To get club details
const getClubDetails = async (req, res) => {
  const { clubName } = req.params;
  const page = Number(req.query.page);
  const limit = Number(req.query.limit);
  const sortBy = req.query.sortby;

  let sortQuery;
  switch (sortBy) {
    case 'new':
      sortQuery = { createdAt: -1 };
      break;
    case 'top':
      sortQuery = { pointsCount: -1 };
      break;
    case 'best':
      sortQuery = { voteRatio: -1 };
      break;
    case 'hot':
      sortQuery = { hotAlgo: -1 };
      break;
    case 'controversial':
      sortQuery = { controversialAlgo: -1 };
      break;
    case 'old':
      sortQuery = { createdAt: 1 };
      break;
    default:
      sortQuery = {};
  }

  const club = await Club.findOne({
    clubName: { $regex: new RegExp('^' + clubName + '$', 'i') },
  }).populate('admin', 'username');

  if (!club) {
    return res.status(404).send({
      message: `Club '${clubName}' does not exist on server.`,
    });
  }

  res.status(200).json({ clubDetails: club });
};

//Fucntion to retrieve top clubs
const getTopClubs = async (_req, res) => {
  const top10Clubs = await Club.find({})
    .sort({ subscriberCount: -1 })
    .limit(3)
    .select('-description -admin');

  res.status(200).json(top10Clubs);
};

// To create new club
const createNewClub = async (req, res) => {
  const { clubName, description, email, phone, university } = req.body;

  const admin = await User.findById(req.user);
  if (!admin) {
    return res
      .status(404)
      .send({ message: 'User does not exist in database.' });
  }

  const existingClubName = await Club.findOne({
    clubName: { $regex: new RegExp('^' + clubName + '$', 'i') },
  });

  if (existingClubName) {
    return res.status(403).send({
      message: `Club having same name "${clubName}" already exists.`,
    });
  }

  const newClub = new Club({
    clubName,
    description,
    email,
    phone,
    university,
    admin: admin._id,
    subscribedBy: [admin._id],
    subscriberCount: 1,
  });

  /*  const subscribedCourse = await Course.find({
     _id: { $in: user.subscribedCourse },
   }); */

  const savedClub = await newClub.save();

  admin.subscribedSubs = admin.subscribedSubs.concat(savedClub._id);
  await admin.save();

  return res.status(201).json(savedClub);
};

// To edit club description
const editClubDescription = async (req, res) => {
  const { description } = req.body;
  const { id } = req.params;

  if (!description) {
    return res
      .status(400)
      .send({ message: `Description body can't be empty.` });
  }

  const admin = await User.findById(req.user);
  const club = await Club.findById(id);

  if (!admin) {
    return res
      .status(404)
      .send({ message: 'User does not exist in database.' });
  }

  if (!club) {
    return res.status(404).send({
      message: `Club with ID: ${id} does not exist in database.`,
    });
  }

  if (club.admin.toString() !== admin._id.toString()) {
    return res.status(401).send({ message: 'Access is denied.' });
  }

  club.description = description;

  await club.save();
  res.status(202).end();
};

// To subscribe a club
const subscribeToClub = async (req, res) => {
  const { id } = req.params;

  const club = await Club.findById(id);
  const user = await User.findById(req.user);

  if (club.subscribedBy.includes(user._id.toString())) {
    club.subscribedBy = club.subscribedBy.filter(
      (s) => s.toString() !== user._id.toString()
    );

    user.subscribedSubs = user.subscribedSubs.filter(
      (s) => s.toString() !== club._id.toString()
    );
  } else {
    club.subscribedBy = club.subscribedBy.concat(user._id);

    user.subscribedSubs = user.subscribedSubs.concat(club._id);
  }

  club.subscriberCount = club.subscribedBy.length;

  await club.save();
  await user.save();

  res.status(201).end();
};

module.exports = {
  getAllClubs,
  getClubDetails,
  createNewClub,
  getTopClubs,
  editClubDescription,
  subscribeToClub,
};
